QT += core
QT += network
QT -= gui

TARGET = Project1

CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

CONFIG += crypto

QMAKE_CXXFLAGS += -std=c++0x -I/usr/local/include #-I/home/rhodys/Final_Project/HElib/src#
QMAKE_LFLAGS += -std=c++0x

LIBS += -I/usr/local/include #-I/home/rhodys/Final_Project/HElib/src# -L/usr/local/lib #/home/rhodys/Final_Project/HElib/src/fhe.a# -lqca-qt5 -lntl -lgmp -lm -lgsl -lgslcblas

SOURCES += src/main.cpp \
    src/databaseclient.cpp \
    src/databaseserver.cpp \
    src/crypto.cpp

HEADERS += src/database.hpp \
    src/crypto.hpp \
    src/databaseserver.hpp \
    src/databaseclient.hpp
