# README

Authors: 
    Ian Dunn
    Patrick Biernat 
    Sam Rhody

Homomorphically Encrypted Database Implementation

# INSTALLATION
=========================================================================================  

QT  
-------  
This project uses the Qt5 framework. This should be available via most package managers or via github.

QCA  
--------  
This project uses QCA which is compatible with Qt5. This may be currently available via package managers but you may need to compile this from github.

NTL  
--------  
HElib requires at least version 6.0.0 of NTL. 
Download it from http://www.shoup.net/ntl/ntl-6.1.0.tar.gz. 

Enter the following commands to install NTL to /usr/include:
    % tar xvf ntl-6.1.0.tar.gz  
    % cd ntl-6.1.0/src  
    % ./configure  
    % make  
    % make check  
    % make install  

This will create a static library file libntl.a.

HElib  
--------  
HElib will be in the repository. If you want to clone it yourself, run
git clone https://github.com/shaih/HElib.git

To install, run  
    cd HElib/src; make

GSL  
--------  
Pearson correlation coefficient calculation is performed using GSL. This is
available from basic package managers.

# COMPILATION  
==================================================================================================  
Use the sample .pro file as a template. You will need to change the LIBS value 
to the location of your installation of HElib. The entries you need to change 
will have # around them. After editing the .pro file, you will need to run  
    qmake
This will compile a suitable Makefile for you to compile the program.


# USAGE
==================================================================================================
Run the program using /path/to/file/Project1

The commands are as follows (all IP addresses are in dotted decimal format):
SourceIP_Average_Msg_Len <Source IP Address> :
    Returns the average length of messages sent from the source IP address. 

DestinationIP_Average_Msg_Len <Destination IP Address> :
    Returns the average length of messages sent to the destination IP address.

From_To_Total#ofMsgs <Source IP Address> <Destination IP Address> :
    Returns the number of messages from the source IP address sent to the 
    destination IP address.

Total_#of_Outbound_Msgs <Source IP Address> :
    Returns the number of messages sent from the source IP address.

Total_#of_Inbound_Msgs <Destination IP Address> :
    Returns the number of messages sent to the destination IP address.a

Pearson_Corr_Coeff_Msgs <IP Address 1> <IP Address 2> :
    Returns the pearson correlation coefficient between the lengths of messages
    involving IP address 1 (as one set of data) and the lengths of messages 
    involving IP address 2 (as a second set of data).

NOTE: All IP addresses are in dotted decimal format.

# COMMENTS
==================================================================================================
It's interesting seeing the current state of homomorphic encryption cryptosystems. Experiencing
the difficulties that FHE designers must overcome to provide something that can be widely used 
provides a good perspective on the evolution of cryptosystems which many people have not been a
part of. It will be interesting to see how cryptosystems can accomodate operations over real 
numbers when it seems as though they are currently much less efficient than systems over integers.