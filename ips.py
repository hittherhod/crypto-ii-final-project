'''
Convert IP to Int
'''
import socket
import struct

def ip2int(addr):
    return struct.unpack("!I", socket.inet_aton(addr))[0]
database = open("./database.dat","r")
info = database.read().split("\n")

for entry in info:
	if (entry == ""):
		break
	entry = entry.split()

	substr = ""
	substr = "{" + hex(ip2int(entry[0])).rstrip('L') + ", " + hex(ip2int(entry[1])).rstrip('L') + ", " + entry[2] + ", " + entry[3] + "},"
	print substr


