#include "databaseserver.hpp"
#include "crypto.hpp"
DatabaseServer::DatabaseServer(QObject *parent) :
    QObject(parent), clientPublicKey(NULL)
{
}

quint32 DatabaseServer::nextAvailableIndex()
{
    if (crypticDatabase.size() == 0)
        return 0;
    return crypticDatabase.size() * crypticDatabase[0].size();
}

void DatabaseServer::appendRow(DB::Row newRow)
{
    crypticDatabase.append(newRow);
} //end appendRow function

DB::IndexedRowList DatabaseServer::findRowsContaining(QPair<DB::Word, QCA::SecureArray> search, qint8 column) {
    QCA::SecureArray k_i = search.second;
    DB::Word clientword = search.first;
    DB::IndexedRowList retlist;

    if(column == -1) {
        for(int i=0;i < crypticDatabase.size();i++) {
            for(int j = 0; j < crypticDatabase.at(i).size();j++) {
                if (Crypto::clientWordMatchesDatabaseWord(clientword,crypticDatabase.at(i).at(j),k_i)) {
                    //Add current row to retlist.
                    DB::Index rowStartIndex = (i * crypticDatabase[0].size());
                    retlist.append(QPair<DB::Index,QList<DB::Word> >(rowStartIndex,crypticDatabase[i]));
                }
            }
        }
    }
    else if ((column != -1) && (column < crypticDatabase[0].size())) {
        for(int i = 0; i < crypticDatabase.size();i++) {
            if(Crypto::clientWordMatchesDatabaseWord(clientword,crypticDatabase[i][column],k_i)) {
                DB::Index index = (i * crypticDatabase[0].size());
                retlist.append(QPair<DB::Index,QList<DB::Word> >(index,crypticDatabase[i]));
            }
        }
    }

    return retlist;
}

DB::RowList DatabaseServer::getAllMsgsFrom(QPair<DB::Word, QCA::SecureArray> sourceIP) {
    DB::IndexedRowList dests = this->findRowsContaining(sourceIP, 0);
    DB::RowList retList;
    foreach(DB::IndexedRow row, dests) {
        retList.append(row.second);
    }
    return retList;
} //end getAllMsgsTo function

DB::RowList DatabaseServer::getAllMsgsTo(QPair<DB::Word, QCA::SecureArray> destIP) {
    DB::IndexedRowList dests = this->findRowsContaining(destIP, 1);
    DB::RowList retList;
    foreach(DB::IndexedRow row, dests) {
        retList.append(row.second);
    }
    return retList;
} //end getAllMsgsTo function

DB::RowList DatabaseServer::fromToGetAllMsgs(QPair<DB::Word, QCA::SecureArray> sourceIP, QPair<DB::Word, QCA::SecureArray> destIP)
{
    DB::IndexedRowList sources = this->findRowsContaining(sourceIP, 0);
    QCA::SecureArray k_i = destIP.second;

    DB::RowList sourceDest;
    foreach(DB::IndexedRow row, sources) {
        if(Crypto::clientWordMatchesDatabaseWord(destIP.first, row.second[1], k_i))
        {
            sourceDest.append(row.second);
        } //end if found matching destination IP
    } //end for each source match

    return sourceDest;
} //end fromToGetAllMsgs function

QPair<Ctxt,Ctxt> DatabaseServer::sourceIPAverageMsgLen(QPair<DB::Word, QCA::SecureArray> sourceIP,EncryptedArray& ea)
{
    DB::RowList source_entries = this->getAllMsgsFrom(sourceIP);
    if (source_entries.size() == 0)
    {
        QTextStream out(stdout, QIODevice::WriteOnly);
        out << "Did not find any messages with this source IP";
        Ctxt ct(*clientPublicKey);
        ea.encrypt(ct, *clientPublicKey, vector<long>(1, 0));
        return QPair<Ctxt,Ctxt>(ct,ct);
    }

    Ctxt avgMsgLen(*clientPublicKey);
    PlaintextArray p(ea);
    p.encode(0);
    ea.encrypt(avgMsgLen, *clientPublicKey, p);

    foreach(DB::Row row, source_entries) {
       Ctxt& message_length = *(new Ctxt(*clientPublicKey));

       memcpy(&message_length, row[3].data(), 128);


       avgMsgLen += message_length;
    } //end for each matching entry
    Ctxt numMsgs(*clientPublicKey);
    PlaintextArray len(ea);
    len.encode(source_entries.size());
    ea.encrypt(numMsgs,*clientPublicKey,len);

    return QPair<Ctxt,Ctxt>(avgMsgLen,numMsgs);
} //end source average message length function


QPair<Ctxt,Ctxt> DatabaseServer::destIPAverageMsgLen(QPair<DB::Word, QCA::SecureArray> destIP,EncryptedArray& ea)
{
    DB::RowList dest_entries = this->getAllMsgsTo(destIP);
    Ctxt entSize(*clientPublicKey);
    PlaintextArray pa(ea);

    pa.encode(dest_entries.size());
    ea.encrypt(entSize, *clientPublicKey, pa);

    if (dest_entries.size() == 0)
    {
        QTextStream out(stdout, QIODevice::WriteOnly);
        out << "Did not find any messages with this dest IP";
        Ctxt ct(*clientPublicKey);
        ea.encrypt(ct, *clientPublicKey, vector<long>(1, 0));
        return QPair<Ctxt,Ctxt>(ct,entSize);
    }

    Ctxt avgMsgLen(*clientPublicKey);

    PlaintextArray p(ea);
    p.encode(0);
    ea.encrypt(avgMsgLen, *clientPublicKey, p);
    foreach(DB::Row row, dest_entries) {
       Ctxt& message_length = *(new Ctxt(*clientPublicKey));

       memcpy(&message_length, row[3].data(), 128);

       avgMsgLen += message_length;
    } //end for each matching entry


    return QPair<Ctxt,Ctxt>(avgMsgLen,entSize);
} //end dest average message length function

Ctxt DatabaseServer::fromToTotalNumOfMsgs(QPair<DB::Word, QCA::SecureArray>& sourceIP, QPair<DB::Word, QCA::SecureArray>& destIP, EncryptedArray& ea) {
    DB::RowList messages = fromToGetAllMsgs(sourceIP, destIP);
    Ctxt enc_num_msg(*clientPublicKey);
    PlaintextArray ptxt(ea);
    ptxt.encode(messages.size());
    ea.encrypt(enc_num_msg, *clientPublicKey, ptxt);
    return enc_num_msg;
} //end get total messages to from function

Ctxt DatabaseServer::totalNumOutMsgs(QPair<DB::Word, QCA::SecureArray>& sourceIP, EncryptedArray& ea) {
    DB::RowList messages = getAllMsgsFrom(sourceIP);
    Ctxt enc_num_msg(*clientPublicKey);
    PlaintextArray ptxt(ea);
    ptxt.encode(messages.size());
    ea.encrypt(enc_num_msg, *clientPublicKey, ptxt);
    return enc_num_msg;
} //end get total messages from function

Ctxt DatabaseServer::totalNumInMsgs(QPair<DB::Word, QCA::SecureArray>& destIP, EncryptedArray& ea) {
    DB::RowList messages = getAllMsgsTo(destIP);
    Ctxt enc_num_msg(*clientPublicKey);
    PlaintextArray ptxt(ea);
    ptxt.encode(messages.size());
    ea.encrypt(enc_num_msg, *clientPublicKey, ptxt);
    return enc_num_msg;
} //end get total messages from function

vector<Ctxt> DatabaseServer::pearsonQuery(QPair<DB::Word, QCA::SecureArray> ipAdd) {
    QTextStream out(stdout, QIODevice::WriteOnly);
    out << "Querying...\n";
    DB::RowList toMsgs = getAllMsgsTo(ipAdd);
    DB::RowList fromMsgs = getAllMsgsFrom(ipAdd);


    vector<Ctxt> msgLengths;
    foreach(DB::Row row, toMsgs) {
       Ctxt& message_length = *(new Ctxt(*clientPublicKey));

       memcpy(&message_length, row[3].data(), 128);
       msgLengths.push_back(message_length);
       qDebug() << msgLengths.size();
    } //end for each matching entry



    foreach(DB::Row row, fromMsgs) {
       Ctxt& message_length = *(new Ctxt(*clientPublicKey));
       memcpy(&message_length, row[3].data(), 128);
       msgLengths.push_back(message_length);

    } //end for each matching entry

    return msgLengths;
}
