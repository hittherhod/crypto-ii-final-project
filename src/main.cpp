#include <QCoreApplication>
#include <QDebug>

#include <QtCrypto/qca.h>
#include <QTextStream>
#include <QtNetwork/QHostAddress> //For IP addresses
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_vector.h>



#include <FHE.h>
#include <EncryptedArray.h>
#include <NTL/lzz_pXFactoring.h>
#include <NTL/ZZ.h>

#include <cstdio>
#include "database.hpp"
#include "databaseclient.hpp"
#include "databaseserver.hpp"
#include "crypto.hpp"

FHEcontext* context;
FHESecKey* secretKey;
FHEPubKey* publicKey;
ZZX G;

void DB::dumpDB(RowList db) {
    foreach (DB::Row r, db) {
        foreach (DB::Word w, r) {
            printf("%s ", w.toHex().data());
        } //end for each word in row
        printf("\n");
    } //end for each row in database
} //end dumpDB function


void setupHELib() {

    QTextStream out(stdout, QIODevice::WriteOnly);
    long p = 34421;
    long r = 1;
    long L = 16;
    long c = 2;
    long k = 128; //Security
    long s = 0;
    long d = 0; //Polynomial degree
    long w = 64;
    long m = FindM(k, L, c, p, d, s, 0);

    out << "+Setting up HElib keys...\n";

    context = new FHEcontext(m, p, r);
    buildModChain(*context, L, c);
    G = context->alMod.getFactorsOverZZ()[0];

    secretKey = new FHESecKey(*context);
    publicKey = secretKey;

    out << "+Created Key Pair\n";
    secretKey->GenSecKey(w);
    addSome1DMatrices(*secretKey);

} //end setupHELib function

char* print_protocol(QByteArray protocol)
{
    quint32 proto_value = protocol.toHex().left(2).toUInt();
    
    if (proto_value == DB::TCP)
    {
        return (char*)"TCP";
    } //end if TCP
    else if (proto_value == DB::DB_LSP_DISC)
    {
        return (char*)"DB_LSP_DISC";
    } //end else if LSP Discovery
    else if (proto_value == DB::SSDP)
    {
        return (char*)"SSDP";
    } //end else if SSDP
    else
    {
        return (char*)"SNMP";
    } //end else SNMP
} //end print_protocol function

void print_usage() {
    cout << "Command List:" << endl;
    cout << "> SourceIP_Average_Msg_Len <Source IP Address>" << endl;
    cout << "> DestinationIP_Average_Msg_Len <Destination IP Address>" << endl;
    cout << "> From_To_Total#ofMsgs <Source IP Address> <Destination IP Address>" << endl;
    cout << "> Total_#of_Outbound_Msgs <Source IP Address>" << endl;
    cout << "> Total_#of_Inbound_Msgs <Destination IP Address>" << endl;
    cout << "> Pearson_Corr_Coeff_Msgs <IP Address 1> <IP Address 2>" << endl;
}


int main(int argc, char** argv)
{
    QCA::Initializer init;
    QTextStream cin(stdin);
    QCoreApplication app(argc, argv);   


    setupHELib();
    EncryptedArray ea(*context,G);
    cout << "+Finished setting up HElib Keys" << endl;

    DatabaseClient alice;
    DatabaseServer bob;

    //Setup Client [Alice] and Database [Bob] Classes.
    DB::RowList rows;    
    foreach(QList<quint32> row, DB::database) {
        rows.append(DB::Row());
        foreach(quint32 entry, row) {
            rows.last().append(QByteArray((char*) &entry, sizeof(quint32)));
        }
    }
    //Encrypt the rows for searching; Homomorphically Encrypting the lengths.
    DB::RowList enc_database = alice.encryptNewRows(rows, 0, ea, publicKey);
    //Send this database to Bob.
    bob.addPublicKey(*publicKey);
    foreach(DB::Row row, enc_database) {
        bob.appendRow(row);
    }

    while(true) {
        QStringList parsed;
        QString input = "";
        cout << "Enter a Command: ";
        input = cin.readLine();
        parsed = input.split(" ");
        vector<long> results, numMsgs;

        if((parsed[0] == "SourceIP_Average_Msg_Len") && (parsed.size() == 2)) {

            quint32 srcIP = QHostAddress(parsed[1]).toIPv4Address();
            QPair<DB::Word, QCA::SecureArray> sourceIP =
                    alice.encryptWordForSearch(QByteArray((char*)&srcIP, sizeof(quint32)));

            QPair<Ctxt,Ctxt> msgLenP = bob.sourceIPAverageMsgLen(sourceIP,ea);
            Ctxt msgLen = msgLenP.first;
            Ctxt num_msgs = msgLenP.second;
            //qDebug() <<  endl << "Sum of MSG Lens. " << endl;

            cout << "Source IP average Message Len" << endl;

            ea.decrypt(msgLen, *secretKey, results);
            ea.decrypt(num_msgs, *secretKey, numMsgs);
            cout << (double)results[0] / (double)numMsgs[0];
        }

        else if ((parsed[0] == "DestinationIP_Average_Msg_Len") && (parsed.size() == 2)) {
            quint32 dstIP = QHostAddress(parsed[1]).toIPv4Address();
            QPair<DB::Word, QCA::SecureArray> destIP =
                    alice.encryptWordForSearch(QByteArray((char*)&dstIP, sizeof(quint32)));

            QPair<Ctxt,Ctxt> msgLenP = bob.destIPAverageMsgLen(destIP,ea);
            Ctxt msgLen = msgLenP.first;
            Ctxt num_msgs = msgLenP.second;

            msgLen = msgLenP.first;
            num_msgs = msgLenP.second;
            ea.decrypt (msgLen, *secretKey, results);
            ea.decrypt (num_msgs, *secretKey, numMsgs);

            cout << "Destintion IP average Message Len: " << endl;
            cout << (double)results[0] / (double)numMsgs[0] << endl;
        }

        else if ((parsed[0] == "From_To_Total#ofMsgs") && (parsed.size() == 3)) {
            quint32 srcIP = QHostAddress(parsed[1]).toIPv4Address();
            QPair<DB::Word, QCA::SecureArray> sourceIP =
                    alice.encryptWordForSearch(QByteArray((char*)&srcIP, sizeof(quint32)));

            quint32 dstIP = QHostAddress(parsed[2]).toIPv4Address();
            QPair<DB::Word, QCA::SecureArray> destIP =
                    alice.encryptWordForSearch(QByteArray((char*)&dstIP, sizeof(quint32)));

            Ctxt num_msgs = bob.fromToTotalNumOfMsgs(sourceIP, destIP, ea);
            ea.decrypt(num_msgs, *secretKey, numMsgs);
            cout << "Number of messages from source to dest IP: " << numMsgs[0] << endl;

        }

        else if ((parsed[0] == "Total_#of_Outbound_Msgs") && (parsed.size() == 2)) {
            quint32 srcIP = QHostAddress(parsed[1]).toIPv4Address();
            QPair<DB::Word, QCA::SecureArray> sourceIP =
                    alice.encryptWordForSearch(QByteArray((char*)&srcIP, sizeof(quint32)));
            Ctxt num_msgs = bob.totalNumOutMsgs(sourceIP, ea);
            ea.decrypt(num_msgs,*secretKey,numMsgs);
            cout << "Total Number of Messages From " << parsed[1].toStdString() << ": " << numMsgs[0] << endl;


        }

        else if ((parsed[0] == "Total_#of_Inbound_Msgs") && (parsed.size() == 2) ) {
            quint32 dstIP = QHostAddress(parsed[1]).toIPv4Address();
            QPair<DB::Word, QCA::SecureArray> destIP =
                    alice.encryptWordForSearch(QByteArray((char*)&dstIP, sizeof(quint32)));

            Ctxt num_msgs = bob.totalNumInMsgs(destIP, ea);
            ea.decrypt(num_msgs,*secretKey,numMsgs);
            cout << "Total Number of Messages To " << parsed[1].toStdString() << ": " << numMsgs[0] << endl;


        }
        else if ((parsed[0] == "Pearson_Corr_Coeff_Msgs") && (parsed.size() == 3) ) {
            quint32 ip1_add = QHostAddress(parsed[1]).toIPv4Address();
            QPair<DB::Word, QCA::SecureArray> ip1 = alice.encryptWordForSearch(QByteArray((char*)&ip1_add, sizeof(quint32)));

            vector<Ctxt> msgLengths = bob.pearsonQuery(ip1);

            //Decrypt the values into a vector of doubles
            vector<double> ip1_lengths;
            foreach(Ctxt message_length, msgLengths) {
                vector<long> ptxt;
                ea.decrypt(message_length, *secretKey, ptxt);
                ip1_lengths.push_back((double) ptxt[0]);
            }

            //Repeat for second 1
            quint32 ip2_add = QHostAddress(parsed[2]).toIPv4Address();
            QPair<DB::Word, QCA::SecureArray> ip2 = alice.encryptWordForSearch(QByteArray((char*)&ip2_add, sizeof(quint32)));

            msgLengths = bob.pearsonQuery(ip2);
            vector<double> ip2_lengths;
            foreach(Ctxt message_length, msgLengths) {
                vector<long> ptxt;
                ea.decrypt(message_length, *secretKey, ptxt);
                ip2_lengths.push_back((double) ptxt[0]);
                //qDebug() << ptxt[0];
            }
            while (ip1_lengths.size() < ip2_lengths.size()) {
                ip1_lengths.push_back(0);
            } //end if pad ip1
            while (ip2_lengths.size() < ip1_lengths.size()) {
                ip2_lengths.push_back(0);
            } //end if pad ip2

            cout << "Pearson Correlation Coefficient between " << parsed[1].toStdString();
            cout << " and " << parsed[2].toStdString() << ": ";
            cout << QString::number(alice.pearsonCorrCoefficientMsgs(ip1_lengths, ip2_lengths)).toStdString();
            cout << "\n";
        } //end if coefficient on messages
        else if (parsed[0] == "exit") {
            exit(0);
        }

        else {
            print_usage();
        }


     }
    return 0;
} //end main function
