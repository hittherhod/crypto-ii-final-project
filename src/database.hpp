#pragma once

namespace DB {
typedef QByteArray Word;
typedef quint32 Index;
typedef QList<Word> Row;
typedef QList<QList<Word> > RowList;
typedef QPair<Index, QList<Word> > IndexedRow;
typedef QList<QPair<Index, QList<Word> > > IndexedRowList;

enum Protocol_Names {TCP, DB_LSP_DISC, SSDP, SNMP};
enum Columns {SourceIP, DestinationIP, Protocol, Length};

const static QList<QList<quint32> > database = {
    {0x6fdd4d9e, 0x81a14b33, TCP, 57},
    {0x81a14b9e, 0xeffffffa, SSDP, 175},
    {0x81a14b9e, 0xeffffffa, SSDP, 175},
    {0xae892a4b, 0x81a14b33, TCP, 66},
    {0xae892a4b, 0x81a14b33, TCP, 66},
    {0x6fdd4d9e, 0x81a14bff, DB_LSP_DISC, 195},
    {0x8d657494, 0x81a14b33, TCP, 54},
    {0x6fdd4d9e, 0x81a14b33, TCP, 57},
    {0x81a14b33, 0xc0a80167, SSDP, 175},
    {0x81a14b33, 0x81a80167, SNMP, 121},
    {0x81a14b33, 0x81a14bff, DB_LSP_DISC, 195},
    {0xae892a4b, 0x81a14b33, TCP, 54},
    {0xae892a4b, 0x81a14b33, SSDP, 175},
    {0xa29ff2a5, 0x81a14b33, TCP, 54},
    {0xae892a4b, 0xc0a80167, TCP, 54},
    {0x6fdd4d9e, 0xc0a80167, SNMP, 121},
    {0x81a14b33, 0xffffffff, DB_LSP_DISC, 195},
    {0x81a14b33, 0xffffffff, DB_LSP_DISC, 195},
    {0x81a14b9e, 0xeffffffa, SSDP, 175},
    {0xae892a4b, 0xeffffffa, SSDP, 175},
    {0xa29ff2a5, 0x81a14b33, TCP, 66},
    {0xa29ff2a5, 0x81a14b33, TCP, 66},
    {0x8d657494, 0x81a14b33, TCP, 54},
    {0x8d657494, 0x81a14b33, TCP, 66}
};

void dumpDB(RowList db);
}
