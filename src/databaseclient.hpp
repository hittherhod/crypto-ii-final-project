#ifndef DATABASECLIENT_H
#define DATABASECLIENT_H

#include <QObject>
#include <QtNetwork/QHostAddress>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_statistics.h>

#include<FHE.h>
#include<EncryptedArray.h>

#include "database.hpp"
#include "crypto.hpp"
class DatabaseClient : public QObject
{
    Q_OBJECT
private:
    const QCA::SecureArray kPrime;
    const QCA::SecureArray kPrimePrime;
    const QCA::InitializationVector preEncryptIV;
    const QCA::SecureArray ks;
    const QCA::SecureArray kk;

    QCA::SecureArray generateTi(DB::Word word, DB::Index index);
public:
    explicit DatabaseClient(QObject *parent = 0);

    /**
     * @brief Encrypts the plaintext newRows to be stored in the database
     * @param newRows The plaintext rows to be encrypted
     * @param nextAvailableIndex The next available index on the server
     *
     * This method takes rows of plaintext data that need to be stored on the server, and encrypts them
     * based on their index in the server, starting with nextAvailableIndex.
     *
     * @return The encrypted rows, which can be appended to the database
     */
    DB::RowList encryptNewRows(DB::RowList newRows, DB::Index nextAvailableIndex);

    /**
     * @brief Overloaded for Final Project. Performs same as above except uses HElib to encrypt message length
     * @param newRows The plaintext rows to be encrypted
     * @param nextAvailableIndex The next available index on the server
     * @param
     *
     * This method takes rows of plaintext data that need to be stored on the server, and encrypts them
     * based on their index in the server, starting with nextAvailableIndex.
     *
     * @return The encrypted rows, which can be appended to the database
     */
    DB::RowList encryptNewRows(DB::RowList newRows, DB::Index nextAvailableIndex, EncryptedArray& ea, FHEPubKey* publicKey);

    /**
     * @brief Decrypt the encrypted crypticRows from the database server for reading
     * @param crypticRows The encrypted and indexed rows from the database server, which are to be
     * decrypted
     *
     * After making a query to the server, it will return an indexed list of encrypted rows. This method
     * will decrypt those rows and return them as plaintext.
     *
     * @return A decrypted version of crypticRows
     */
    DB::RowList decryptRows(DB::IndexedRowList crypticRows);

    /**
     * @brief Encrypts the word plainText for searching within the database server
     * @param plainText The plaintext word which is to be searched for in the database
     *
     * In order for the database server to search for a word, that word must be encrypted. This method
     * will take a plaintext word and encrypt it so that the database server can search for it.
     *
     * @return An encrypted version of plainText which the server can search the database for
     */
    QPair<DB::Word, QCA::SecureArray> encryptWordForSearch(DB::Word plainText);

    /**
     * @brief Computes pearson correlation coefficient given lengths of messages from each IP address
     * @param IP1msgs Lengths of messages from IP address 1
     * @param IP2msgs Lengths of messages from IP address 2
     *
     * Takes results from DB query and computes pearson correlation coefficient
     *
     * @return Pearson correlation coefficient between two data sets
     */
    qreal pearsonCorrCoefficientMsgs(vector<double>& IP1msgs, vector<double>& IP2msgs);

    /**
     * @brief Computes pearson correlation coefficient given lengths of messages from each IP address
     * @param address1 First IP address in quint32 format
     * @param address2 Second IP address in quint32 format
     *
     * Takes IP addresses and computes correlation between them as bitwise vectors.
     *
     * @return Pearson correlation coefficient between two data sets
     */
    qreal pearsonCorrCoefficientIP(quint32 address1, quint32 address2);

signals:

public slots:

};

#endif // DATABASECLIENT_H
