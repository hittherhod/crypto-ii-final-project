#ifndef DATABASESERVER_H
#define DATABASESERVER_H

#include <FHE.h>
#include <EncryptedArray.h>
#include <NTL/ZZ.h>

#include <QObject>
#include <QtCrypto/qca.h>
#include <QTextStream>
#include <QDebug>
#include "database.hpp"

class DatabaseServer : public QObject
{
    Q_OBJECT
public:
    explicit DatabaseServer(QObject *parent = 0);

    /**
     * @brief Return the next available index within the database
     *
     * This calculates the next available index, which is the same as the number
     * of items currently stored. If there are two rows in the database, four
     * items per row, then the next available index is 8. If the database is
     * empty, the next available index is 0.
     *
     * This information is needed by the client, in order to add new rows to the
     * database, as the encryption of a field is based on its index within the
     * database.
     *
     * @return The next available index
     */
    DB::Index nextAvailableIndex();

    /**
     * @brief Adds a new row to the database
     * @param newRow The new row to add
     *
     * This method adds a new row to the end of the database. The row should be
     * properly encrypted already, and must have taken nextAvailableIndex() into
     * account.
     */
    void appendRow(DB::Row newRow);

    /**
     * @brief Return all rows containing the specified word
     * @param word The word to search for
     * @param column The column to search for the word in; if -1, search all
     *
     * This method searches the encrypted database for word. If column is not
     * -1, then only the specified column is searched, but the entire matching
     * row will be returned. If column is -1, then all columns are searched.
     *
     * If column is less than -1 or greater than the highest column index, the
     * return value is QList<QList<quint32> >()
     *
     * @return Indexed list of all rows containing the specified word
     */
    DB::IndexedRowList findRowsContaining(QPair<DB::Word, QCA::SecureArray> search, qint8 column = -1);

    /**
     * @brief Returns average message length given only source IP
     * @param src_ip Encrypted source IP address
     *
     * This function takes an encrypted source IP address, searches for it in
     * the database, constantly summing up the message lengths homomorphically,
     * and returns the average.
     *
     * @return Average message length of messages sent from specified source IP
     */
  QPair<Ctxt,Ctxt> sourceIPAverageMsgLen(QPair<DB::Word, QCA::SecureArray> sourceIP,EncryptedArray& ea);

    /**
     * @brief Returns average message length given only destination IP
     * @param src_ip Encrypted destination IP address
     *
     * This function takes an encrypted destination IP address, searches for it in
     * the database, constantly summing up the message lengths homomorphically,
     * and returns the average.
     *
     * @return Average message length of messages sent from specified destination IP
     */
  QPair<Ctxt,Ctxt> destIPAverageMsgLen(QPair<DB::Word, QCA::SecureArray> destIP,EncryptedArray& ea);

  /**
   * @brief Gets total number of messages from source to dest
   * @param sourceIP Source IP pre-encrypted
   * @param destIP Destination IP pre-encrypted
   * @param ea EncryptedArray for creating ciphertexts
   * 
   * @return Encrypted number of messages from source to dest
   */
  Ctxt fromToTotalNumOfMsgs(QPair<DB::Word, QCA::SecureArray>& sourceIP, QPair<DB::Word, QCA::SecureArray>& destIP, EncryptedArray& ea);

  /**
   * @brief Gets total number of messages from source
   * @param sourceIP Source IP pre-encrypted
   * @param ea EncryptedArray for creating ciphertexts
   * 
   * @return Encrypted number of messages from source
   */
  Ctxt totalNumOutMsgs(QPair<DB::Word, QCA::SecureArray>& sourceIP, EncryptedArray& ea);

  /**
   * @brief Gets total number of messages to destination
   * @param destIP Destination IP pre-encrypted
   * @param ea EncryptedArray for creating ciphertexts
   * 
   * @return Encrypted number of messages to dest
   */
  Ctxt totalNumInMsgs(QPair<DB::Word, QCA::SecureArray>& destIP, EncryptedArray& ea);
    /**
     * @brief Gets all messages sent from the source IP
     * @param sourceIP Source IP address pre-encrypted
     *
     * Returns a list of all messages sent from an IP address
     *
     * @return List of all rows which have srcIP as the source IP value
     */
    DB::RowList getAllMsgsFrom(QPair<DB::Word, QCA::SecureArray> sourceIP);

    /**
     * @brief Gets all messages sent to the dest IP
     * @param destIP Destination IP address pre-encrypted
     *
     * Returns a list of all messages sent to an IP address
     *
     * @return List of all rows which have destIP as the destination IP value
     */
    DB::RowList getAllMsgsTo(QPair<DB::Word, QCA::SecureArray> destIP);

    /**
     * @brief Gets all messages sent from source IP to destination IP
     * @param sourceIP Source IP pre-encrypted
     * @param destIP Destination IP pre-encrypted
     *
     * First gets all messages containing the sourceIP, then finds all messages
     * in that which contain destIP as the destinationIP
     *
     * @return List of all rows which have sourceIP as source and destIP as dest
     */
    DB::RowList fromToGetAllMsgs(QPair<DB::Word, QCA::SecureArray> sourceIP,
                                            QPair<DB::Word, QCA::SecureArray> destIP);

    /**
     * @brief Returns all necessary information for one part of a pearson corr query for messages
     * @param ipAdd IP address pre-encrypted
     *
     * Queries the database, returning the message lengths encrypted as a vector
     *
     * @return Vector containing message lengths of all messages to and from the ipAdd
     */
    vector<Ctxt> pearsonQuery(QPair<DB::Word, QCA::SecureArray> ipAdd);

    /**
     * @brief Adds new public key to public key storage
     * @param newPubKey New public key to be added
     *
     * Just sets the clientPublicKey variable to be newPubKey
     */
    void addPublicKey(FHEPubKey& newPubKey) {
        if(clientPublicKey != NULL) {
            delete(clientPublicKey);
        }
        clientPublicKey = &newPubKey;
    }

signals:

public slots:

private:
    QList<QList<DB::Word> > crypticDatabase;
    FHEPubKey* clientPublicKey;
};

#endif // DATABASESERVER_H
